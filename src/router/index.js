import Vue from 'vue'

import Home from '@/components/Home'
import Services from '@/components/Services'
import Service from '@/components/Service'
import Merchant from '@/components/Merchant'
import Search from '@/components/Search'
import Cart from '@/components/Cart'
import GmapSelector from '@/components/GmapSelector'
import Orders from '@/components/Orders'
import OrderDetail from '@/components/OrderDetail'
import Profile from '@/components/Profile'
import ProfileEdit from '@/components/ProfileEdit'
import Favorites from '@/components/Favorites'
import Notifications from '@/components/Notifications'
import Login from '@/components/Login'
import Register from '@/components/Register'

import {store} from '../store/store'

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next()
    return
  }
  next('/')
}

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next()
    return
  }
  next('/login')
}

export default [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    beforeEnter: ifNotAuthenticated,
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    beforeEnter: ifNotAuthenticated,
  },
  {
    path: '/services',
    name: 'Services',
    component: Services,
  },
  {
    path: '/service/:id',
    name: 'Service',
    component: Service,
  },
  {
    path: '/merchant/:id',
    name: 'Merchant',
    component: Merchant,
  },
  {
    path: '/search',
    name: 'Search',
    component: Search,
  },
  {
    path: '/cart',
    name: 'Cart',
    component: Cart,
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/gmap',
    name: 'GmapSelector',
    component: GmapSelector,
    // beforeEnter: ifAuthenticated,
  },
  {
    path: '/orders',
    name: 'Orders',
    component: Orders,
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/order/:id',
    name: 'OrderDetail',
    component: OrderDetail,
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/favorites',
    name: 'Favorites',
    component: Favorites,
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/profile/edit',
    name: 'ProfileEdit',
    component: ProfileEdit,
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/notifications',
    name: 'Notifications',
    component: Notifications,
    beforeEnter: ifAuthenticated,
  }
];




