// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import VueRouter from 'vue-router'
import theRoutes from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import 'es6-promise/auto'
import VeeValidate from 'vee-validate';
import Vuelidate from 'vuelidate'
import * as VueGoogleMaps from "vue2-google-maps";
import VueGeolocation from 'vue-browser-geolocation';

import { store } from './store/store'

window.Vue = Vue;

Vue.use(Vuetify, {
  theme: {
    primary: '#e65100',
    secondary: '#b0bec5',
    accent: '#e65100',
    error: '#b71c1c'
  }
});
Vue.use(VueRouter)
Vue.use(VueAxios, axios);

Vue.use(VeeValidate);
Vue.use(Vuelidate);
Vue.use(VueGeolocation);
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyCgJ4vLIObURgCP_R7Hb6OpRvqpZ6tAPrs",
    libraries: "places", // necessary for places input
  }
});
// use moment to filter time
Vue.use(require('vue-moment'));
//if has token
const token = localStorage.getItem('_token')

if (token) {
    // Alter defaults after instance has been created
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
  // axios.defaults.baseURL = 'http://localhost/kolos/public/api/v1';
  axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
}


// the routes
const router = new VueRouter({
  mode: 'history',
  routes: theRoutes
});

Vue.config.productionTip = false

/* eslint-disable no-new */
const app = new Vue({
  el: '#app',
  router, store,
  components: { App },
  template: '<App/>'
}).$mount('#app');
