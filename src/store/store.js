import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
const BASE_URL = 'https://kolos.co.id/api/v1/';
export const store = new Vuex.Store({
    strict: true,
    state: {
        preRegisterData: [],
        myLocation: [],
        user: [],
        sliders: [],
        topCategories: [],
        categories: [],
        merchants: [],
        merchant: [],
        merchantServices: [],
        merchantReviews: [],
        cart: [],
        orderLocation: [],
        orderDetail: [],
        orderStatus: '',
        notifications: [],
        notificationsNum: '',
        // authentication
        token: localStorage.getItem('_token') || '',
        myFavorites: [],
        favorited: false,
        endpoints: {
            register: BASE_URL + 'register-new',
            login: BASE_URL + 'login',
            refreshJWT: 'http://0.0.0.0:10000/auth/refresh_token', // in development
            getSliders: BASE_URL + 'slideshow/all',
            getTopCategories: BASE_URL + 'category/top',
            getMerchants: BASE_URL + 'category/detail',
            getCategories: BASE_URL + 'category/all',
            getMerchantDetail: BASE_URL + 'merchant/detail',
            processOrder: BASE_URL + 'order/process-order',
            getOrder: BASE_URL + 'order/detail-new',
            cancelOrder: BASE_URL + 'order/cancel',
            getFavorites: BASE_URL + 'merchant/favorites',
            addToFavorite: BASE_URL + 'merchant/addToFavorite',
            removeFromFavorite: BASE_URL + 'merchant/removeFromFavorite',
            getNotifications: BASE_URL + 'alert/get',
            readNotification: BASE_URL + 'alert/read',
            getUser: BASE_URL + 'getuser',
            editUser: BASE_URL + 'user/edit-new',
            // uploadAvatar: BASE_URL + 'user/upload/avatar'
        },
    },
    getters: {
        //authentication
        isAuthenticated: state => !!state.token,
        authStatus: state => state.status,
        isFavorited: state => state.favorited,
        orderStatus: state => state.orderStatus,
        notificationsNum: state => state.notificationsNum,
        // shopping cart
        getPreRegisterData: state => state.preRegisterData,
        getCart: state => {
            return state.cart;
        },
        getLocation: state => {
            return state.orderLocation;
        },  
    },
    mutations: {
        preRegister(state, data){
            state.preRegisterData = data
        },
        setLocation(state, locations){
            state.myLocation = locations
        },
        // authentication
        updateToken(state, newToken){
            localStorage.setItem('_token', newToken);
            state.token = newToken;
            state.status = 'authenticated';
        },
        removeToken(state){
            localStorage.removeItem('_token');
            state.token = null;
            state.myFavorites = [];
            state.notifications = [];
            state.notificationsNum = '';
        },
        // user
        user(state, user) {
            state.user = user
        },
        sliders(state, sliders){
            state.sliders = sliders
        },
        topCategories(state, categories){
            state.topCategories = categories
        },
        categories(state, categories){
            state.categories = categories
        },
        merchants(state, merchants){
            state.merchants = merchants
        },
        // get merchant detail
        merchant(state, merchant) {
            state.merchant = merchant;
        },
        // get merchant services
        merchantServices(state, merchantServices) {
            state.merchantServices = merchantServices;
        },
        // get merchant reviews
        merchantReviews(state, merchantReviews) {
            state.merchantReviews = merchantReviews;
        },
        // favorites
        myFavorites(state, favorites) {
            state.myFavorites = favorites;
        },
        orderDetail(state, order) {
            state.orderDetail = order
        },
        orderStatus(state, order) {
            state.orderStatus = order
        },
        notifications(state, notifications) {
            state.notifications = notifications
        },
        notificationsNum(state) {
            let num = 0;
            for(let i = 0; i < state.notifications.length; i++) {
                if(state.notifications[i].read === 0) {
                    num += 1;
                }
            }
            state.notificationsNum = num;
        },
        readNotification(state, notification){
            for(let i = 0; i < state.notifications.length; i++) {
                if(state.notifications[i].id === notification) {
                    state.notifications[i].read = 1;
                    state.notificationsNum = state.notificationsNum - 1;
                    break;
                }
            }
        },
        addToFavorite(state, merchant) {
            state.favorited = true;
        },
        removeFromFavorite(state, merchant) {
            state.favorited = false;
        },
        // check favorited or no
        checkFavorite(state, merchant) {
            let found = false;
            for(let i = 0; i < state.myFavorites.length; i++) {
                if(state.myFavorites[i].merchant_id === merchant) {
                    found = true;
                    break;
                }
            }

            if(found){
                state.favorited = true;
            } else {
                state.favorited = false;
            }
        },
        // add to cart
        addToCart(state, service) {
            let found = false;
            for(let i=0; i < state.cart.length; i++){
                if(state.cart[i].merchant_id !== service.user_id){
                    state.cart.splice(state.cart.indexOf(state.cart[i]));
                    found = false;
                    break;
                }
                if(state.cart[i].service_id === service.id){
                    state.cart[i].quantity += 1;
                    found = true;
                    break;
                }
            }

            if(!found){
                state.cart.push(
                    {
                        "service_id" : service.id,
                        "merchant_id" : service.user_id,
                        "quantity" : 1,
                        "name" : service.name,
                        "price" : service.price
                    }
                );
            }
        },
        // remove item from cart
        removeFromCart(state, item) {
            let p = state.cart[state.cart.indexOf(item)];
            if(p.quantity === 1) {
                
                state.cart.splice(state.cart.indexOf(item), 1);
                return;
            }
            p.quantity -= 1;
        },

        // add location
        addLocation(state, location) {

            if(location === null) {
                alert('Please select location');
                return;
            }
            state.orderLocation.splice(state.orderLocation[0], 1);
            state.orderLocation.push(
                {
                    "loc_name"  : location.name,
                    "loc_addr"  : location.formatted_address,
                    "lon"       : location.geometry.location.lng(),
                    "lat"       : location.geometry.location.lat(),
                }
            );
            
        },

        
    },
    actions: {
        setLocation({commit}, locations){
            commit('setLocation', locations);
        },
        preRegister({commit}, data) {
            commit('preRegister', data)
        },
        register ({commit}, data) {
            return new Promise((resolve, reject) => {
                const payload = {
                    name: data.name,
                    email: data.email,
                    phone: data.phone,
                    password: data.password,
                    home_address: data.home_address,
                    home_description: data.home_description,
                    home_latitude: data.home_latitude,
                    home_longitude: data.home_longitude
                }
                
                axios.post(this.state.endpoints.register, payload)
                    .then((response)=>{
                        resolve(response)
                    })
                    .catch((error)=>{
                        reject(error)
                    })
            })
        },
        // authentication
        login(commit, data){
            return new Promise((resolve, reject) => {

                const payload = {
                    email: data.email,
                    password: data.password
                }
                
                axios.post(this.state.endpoints.login, payload)
                    .then((response)=>{
                        const token = response.data.data.token
                        // update token in localstorage and state
                        this.commit('updateToken', response.data.data.token);
                        // add token into axios header
                        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
                        resolve(response)
                    })
                    .catch((error)=>{
                        reject(error)
                    })
            })
        },
        // edit user
        editUser(commit, data){
            return new Promise((resolve, reject) => {

                const payload = {
                    name: data.name,
                    phone: data.phone,
                    avatar: data.avatar
                }
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.state.token;
                axios.post(this.state.endpoints.editUser, payload)
                    .then((response)=>{
                        this.commit('user', response.data.user);
                        resolve(response)
                    })
                    .catch((error)=>{
                        reject(error)
                    })
            })
        },
        // logout
        logout(commit) {
            return new Promise((resolve, reject) => {
                localStorage.removeItem('_token')
                // remove the axios default header
                this.commit('removeToken');
                delete axios.defaults.headers.common['Authorization']
                resolve()
            })
        },
        refreshToken(){
            const payload = {
                token: this.state.jwt
            }
            axios.post(this.state.endpoints.refreshJWT, payload)
                .then((response)=>{
                    this.commit('updateToken', response.data.token)
                })
                .catch((error)=>{
                    console.log(error)
                })
        },
        loadSliders({commit}) {
            return new Promise((resolve, reject) => {
                axios.get(this.state.endpoints.getSliders)
                .then((response) => {
                    commit('sliders', response.data.data);
                    resolve(response)
                }).catch( error => { 
                    reject(error)
                })
            })
        },
        loadTopCategories({commit}) {
            return new Promise((resolve, reject) => {
                axios.get(this.state.endpoints.getTopCategories)
                .then((response) => {
                    commit('topCategories', response.data.data);
                    resolve(response)
                }).catch( error => { 
                    reject(error)
                })
            })
        },
        loadCategories({commit}) {
            return new Promise((resolve, reject) => {
                axios.get(this.state.endpoints.getCategories)
                .then((response) => {
                    commit('categories', response.data.data);
                    resolve(response)
                }).catch( error => { 
                    reject(error)
                })
            })
        },
        // get all merchants in a category
        loadMerchants({commit}, id) {
            return new Promise((resolve, reject) => {
                axios.get(this.state.endpoints.getMerchants, {
                    params: {
                        id: encodeURIComponent(id)
                    }
                })
                .then((response) => {
                    commit('merchants', response.data.users);
                    resolve(response)
                }).catch( error => { 
                    reject(error)
                })
            })
        },
        // action get merchant informations
        loadMerchant (context, id) {
            return new Promise((resolve, reject) => {
                axios.get(this.state.endpoints.getMerchantDetail, {
                    params: {
                        id: encodeURIComponent(id)
                    }
                }).then((response) => {
                    context.commit('merchant', response.data.merchant);
                    context.commit('merchantServices', response.data.services);
                    context.commit('merchantReviews', response.data.reviews);
                    context.commit('checkFavorite', id);
                    resolve(response)
                }).catch( error => { 
                    reject(error)
                })
            })
        },
        // action add to cart
        addToCart ({commit}, service){
            commit('addToCart', service);
        },
        removeFromCart ({commit}, item) {
            commit('removeFromCart', item);
        },
        // add order location
        addLocation ({commit}, location) {
            commit('addLocation', location);
        },
        // favorite merchants
        loadFavorites ({commit}, payload) {
            return new Promise((resolve, reject) => {
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.state.token;
                axios.get(this.state.endpoints.getFavorites)
                .then((response) => {
                    //console.log(response)
                    if(response.data.status !== 201) {
                        commit('myFavorites', response.data.favorites);
                    }
                    commit('myFavorites', '');
                    resolve(response)
                }).catch( error => { 
                    reject(error)
                })
            })
        },
        // check if current merchant is my favorite
        checkFavorite ({commit}, merchant) {
            commit('checkFavorite', merchant);
        },
        // add this merchant to my favorite
        addToFavorite ({commit}, merchant) {
            return new Promise((resolve, reject) => {
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.state.token;
                const payload = {
                    merchant_id: merchant,
                }
                axios.post(this.state.endpoints.addToFavorite, payload)
                    .then((response) => {
                        commit('addToFavorite', merchant);
                        resolve(response)
                    })
                    .catch((error) => {
                        reject(error)
                    })
            })
        },
        // remove this merchant from my favorite
        removeFromFavorite ({commit}, merchant) {
            return new Promise((resolve, reject) => {
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.state.token;
                const payload = {
                    merchant_id: merchant,
                }
                axios.post(this.state.endpoints.removeFromFavorite, payload)
                    .then((response) => {
                        commit('removeFromFavorite', merchant);
                        resolve(response)
                    })
                    .catch((error) => {
                        reject(error)
                    })
            })
        },
        // make an order / service
        placeOrder (commit, data) {
            return new Promise((resolve, reject) => {

                const payload = {
                    merchant_id: data.merchant_id,
                    booking_time: data.booking_time,
                    location: data.location,
                    location_detail: data.location_detail,
                    lon: data.lon,
                    lat: data.lat,
                    services: data.services,
                    note: data.note
                }

                axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.state.token;
                axios.post(this.state.endpoints.processOrder, payload)
                    .then((response)=>{
                        resolve(response)
                    })
                    .catch((error)=>{
                        reject(error)
                    })
            })
        },
        // get order detail
        loadOrder({commit}, id) {
            return new Promise((resolve, reject) => {
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.state.token;
                axios.get(this.state.endpoints.getOrder, {
                    params: {
                        id: encodeURIComponent(id)
                    }
                })
                .then((response) => {
                    commit('orderDetail', response.data.order);
                    commit('orderStatus', response.data.order.status)
                    resolve(response)
                }).catch( error => {
                    reject(error)
                })
            })
        },

        // cancel order
        cancelOrder({commit}, id) {
            return new Promise((resolve, reject) => {
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.state.token;
                const payload = {
                    id: id
                }
                axios.post(this.state.endpoints.cancelOrder, payload)
                .then((response) => {
                    commit('orderStatus', 3)
                    resolve(response)
                }).catch( error => {
                    reject(error)
                })
            })
        },

        // get order detail
        loadNotifications({commit}) {
            return new Promise((resolve, reject) => {
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.state.token;
                axios.get(this.state.endpoints.getNotifications)
                .then((response) => {
                    commit('notifications', response.data.alert);
                    commit('notificationsNum')
                    resolve(response)
                }).catch( error => {
                    reject(error)
                })
            })
        },

        // set notifications read
        readNotification({commit}, notification) {
            return new Promise((resolve, reject) => {
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.state.token;
                const payload = {
                    id: notification
                }
                axios.post(this.state.endpoints.readNotification, payload)
                .then((response) => {
                    commit('readNotification', notification);
                    // commit('notificationsNum')
                    resolve(response)
                }).catch( error => {
                    reject(error)
                })
            })
        },

        // get user
        getUser({commit}) {
            return new Promise((resolve, reject) => {
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.state.token;
                axios.post(this.state.endpoints.getUser)
                .then((response) => {
                    commit('user', response.data.data);
                    // console.log(response.data.data)
                    resolve(response)
                }).catch( error => {
                    reject(error)
                })
            })
        }, 
        // upload avatar
        uploadAvatar({commit}, payload) {
            return new Promise((resolve, reject) => {
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.state.token;
                axios.post(this.state.endpoints.uploadAvatar)
                .then((response) => {
                    resolve(response)
                }).catch( error => {
                    reject(error)
                })
            })
        }

    }
})